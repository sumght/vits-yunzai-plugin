import Version from './Version.js'

const Path = process.cwd()
export {
    Version,
    Path
}
